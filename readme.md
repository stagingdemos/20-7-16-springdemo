# Spring Example

This demo used spring framework to create an app that would show off how to configure your application context through both xml and annotations. The different features of the application were created in feature branches that help keep track of the development of this application. It was done this way in order to give you guys some practice with git. To show the evolution of this project I would recommend making a branch and then merging other branches into it in the following order:

- init
- swf-with-annotations (or you could merge in swf-with-xml and then roll it back)
- orm-setup
- orm-refactor

If you would like to see the completed application, you can just checkout the orm-refactor branch.

*As a Side note, Hibernate 4 was used for this demo*
## SQL Script
Here is the script for creating the table in your DB:
```sql
drop table if exists book;
create table book(
	id serial,
	title varchar,
	author varchar,
	genre varchar
);

select * from book;
```
